* Fonctionnalité de translation : 
développer un script de translation automatique en utilisant le package "translator" disponible sur "pub.dev". En exploitant ce package
basé sur l'api de Google Translate, j'ai réalisé une méhode qui retourne un Widget dont le texte contenu est traduit vers la langue 
choisie. En fait, 3 trois langues sont disponibles comme convenu : l'anglais par défaut, le français et l'arabe. 

* Rubrique des évènements :
Cette fonctionnalité permet d'ajouter des évènements, organisés ou à venir, par les superviseurs de scouts. Ils doivent indiquer le 
titre, la date, la description et une image qui illustre l'évènement en question. L'évènement est ensuite sauvegardé dans la base de
données et ajouté dans l'application.

* Rubrique Team :
Cette rubrique contient un hommage à l'équipe des scouts de Sfax qui a proposé l'idée de l'application mobile et nous a fourni toutes
les informations nécessaires pour la réussite de notre tâche. Nous avons cité tous les membres de cette équipe accompagnés de leurs
postes administratifs.