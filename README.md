Camping Academy (أكاديمية التخييم) est une application mobile développée par des jeunes élèves ingénieurs à l'École Supérieure des 
Communications de Tunis (SUP'COM). L'idée de l'application est proposée par « les scouts de Sfax » : digitaliser toutes les activités 
principales des scouts tunisiens pour une performance et une traçabilité meilleure.
Camping Academy constitue un nouveau palier dans le monde des scouts incitant les jeunes tunisiens à s'inscrire dans les nombreuses 
escouades de scouts. 
L'application fournit plusieurs fonctionnalités aux superviseurs et membres des scouts :

- Consulter la carte de localisation (la localisation actuelle se manifeste par défaut)

- Consulter la météo des différentes villes tunisiennes avec des prévisions pour les cinq prochains jours

- Communiquer avec les collègues des scouts et échanger des photos et des moments inoubliables

- Ajouter et partager des événements organisés par l'escouade des scouts

- Publier des photos de sorties et visites dans la rubrique "souvenirs"

- Afficher les différentes phases (explorateur, aventurier, nomade) des scouts et les badges associés

- Consulter les étapes nécessaires pour l'obtention des badges et soumettre le travail requis pour chaque étape

Il s'agit d'une version primaire; des mises à jour peuvent suivre le lancement dans l'avenir.

Le fichier .apk se trouve dans le chemin d'accès : build/app/outputs/flutter-apk/app.apk

Merci pour votre attention et n'oubliez pas de rêver très grand ❤