import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MessageBubble extends StatefulWidget {
  MessageBubble(this.message, this.userName,this.isMe,this.image);

  final String message;
  final String userName;
  final bool isMe;
  var image;

  @override
  State<MessageBubble> createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
    Row(
    mainAxisAlignment: !widget.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              color: !widget.isMe ? Colors.grey[300] : Theme.of(context).accentColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(14),
                topRight: Radius.circular(14),
                bottomLeft: widget.isMe ? Radius.circular(0) : Radius.circular(14),
                bottomRight: !widget.isMe ? Radius.circular(0) : Radius.circular(14),
              )
          ),
          width: MediaQuery.of(context).size.width/3,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
          margin: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
            child: Column(
              crossAxisAlignment:
              !widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
              children: [
                Text(
                  widget.userName + " :",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,

                    color: !widget.isMe
                        ? Colors.blue
                        : Colors.white,
                  ),
                ),
                
                Text(
                  widget.message,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: !widget.isMe
                        ? Colors.black
                        : Colors.black
                  ),
                  textAlign: !widget.isMe ? TextAlign.end : TextAlign.start,
                ),
              ],
            ),
          )
          ],
        ),
Positioned(
            top: 0,
            left: widget.isMe ? 20 : null,
            right: widget.isMe ? MediaQuery.of(context).size.width/3 : MediaQuery.of(context).size.width/3-10,
            child: Container(
                width: 40,
                height: 40,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: ClipRRect(
                    borderRadius:
                        BorderRadius.circular(400),
                    child: Image.network(widget.image),
                  ),
                ),
              ),

            
        ),
      ],
      overflow: Overflow.visible,
    );
  }
}